﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class Ticket
    {
        public Ticket()
        {
            Id = Guid.NewGuid();
            CreationDateTime = DateTime.Now;
        }

        [Required]
        public Guid Id { get; }

        [Required]
        public bool Urgent { get; set; }

        [Required]
        [Display(Name = "Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Due Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime DueDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Time of submission")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CreationDateTime { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Description == null)
            {
                yield return new ValidationResult("Description cannot be empty.", new[] { "Description" });
            }
            if (DueDate < DateTime.Now)
            {
                yield return new ValidationResult("Due date cannot be in the past.", new[] { "DueDate" });
            }
        }
    }
}
