﻿using System;
using System.Collections.Generic;

namespace DataAccessLayer
{
    class DataRepository
    {
        private static List<Ticket> _tickets = new List<Ticket>();

        public static List<Ticket> Tickets
        {
            get
            {
                SetUrgency();
                return _tickets;
            }
            set { _tickets = value; }
        }

        private static void SetUrgency()
        {
            Tickets.ForEach(x => { x.Urgent = (x.DueDate - DateTime.Now).TotalMinutes < 60; });
        }
    }
}
