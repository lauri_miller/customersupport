﻿/*
using System;
using System.ComponentModel.DataAnnotations;
using CustomerSupport.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CustomerSupport.Tests
{
    [TestClass]
    public class ValidationTest
    {
        [TestMethod]
        [ExpectedException(typeof(ValidationException), "Description cannot be empty.")]
        public void Validate_Should_Fail_When_DescriptionIsNull()
        {
            var ticketViewModel = new CustomerSupport.Models.TicketViewModel()
            {
                Description = null,
                DueDate = DateTime.Now
            };

            var validationContext = new ValidationContext(ticketViewModel);

            Validator.ValidateObject(ticketViewModel, validationContext);
        }

        [TestMethod]
        [ExpectedException(typeof(ValidationException), "Due date cannot be in the past.")]
        public void Validate_Should_Fail_When_DueDateIsInPast()
        {
            var ticketViewModel = new CustomerSupport.Models.TicketViewModel()
            {
                Description = "Test",
                DueDate = DateTime.Now.AddMinutes(-5)
            };

            var validationContext = new ValidationContext(ticketViewModel);

            Validator.ValidateObject(ticketViewModel, validationContext);
        }

        [TestMethod]
        public void Validate_Should_Pass_When_ModelIsValid()
        {
            var ticketViewModel = new CustomerSupport.Models.TicketViewModel()
            {
                Description = "Test",
                DueDate = DateTime.Now.AddMinutes(5)
            };

            var validationContext = new ValidationContext(ticketViewModel);

            Validator.ValidateObject(ticketViewModel, validationContext);
        }
    }
}
*/
