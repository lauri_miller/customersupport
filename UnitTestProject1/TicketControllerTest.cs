﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BusinessLogic;
using CustomerSupport.Controllers;
using CustomerSupport.Models;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Api;
using NUnit.Framework.Internal;
using Assert = NUnit.Framework.Assert;

namespace CustomerSupport.Tests
{
    [TestFixture]
    public class TicketControllerTest
    {
        private Mock<IRepository> _repository;
        private Mock<IViewModelProvider> _viewModelProvider;
        private Mock<ITicketViewModelToDomainModelMapper> _mapper;
        private TicketController _controller;

        [SetUp]
        public void Setup()
        {
            _repository = new Mock<IRepository>();
            _mapper = new Mock<ITicketViewModelToDomainModelMapper>();
            _viewModelProvider = new Mock<IViewModelProvider>();
            _controller = new TicketController(_repository.Object, _viewModelProvider.Object, _mapper.Object);
        }

        [Test]
        public void Create_Should_Return_Edit_View_If_ModelStateInvalid()
        {
            // Arrange
            _controller.ModelState.AddModelError("TestError", @"ErrorMessage");
            var viewModel = new CreateTicketViewModel();

            // Act
            var result = _controller.Create(viewModel) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.ViewName);
            _repository.Verify(r => r.AddTicket(It.IsAny<Ticket>()), Times.Never);
        }

        [Test]
        public void Create_Should_Access_Mapper_And_Call_AddTicket_When_ModelStateIsValid()
        {
            // Arrange
            var viewModel = new CreateTicketViewModel();
            var ticket = new Ticket();
            _mapper.Setup(m => m.MapCreateViewModel(viewModel)).Returns(ticket);

            // Act
            _controller.Create(viewModel);

            // Assert
            _mapper.Verify(m => m.MapCreateViewModel(viewModel), Times.Once);
            _repository.Verify(r => r.AddTicket(ticket), Times.Once);
        }

        [Test]
        public void Edit_Should_Return_Index_When_Called_Without_Id()
        {
            // Act
            var result = _controller.Edit((Guid?)null) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.ViewName);
            _repository.Verify(r => r.GetTicket(It.IsAny<Guid>()), Times.Never);
            _viewModelProvider.Verify(p => p.ProvideEditTicketViewModel(It.IsAny<Ticket>()), Times.Never);
        }

        [Test]
        public void Edit_Should_Return_View_With_Correct_ViewModel()
        {
            // Arrange
            var id = Guid.NewGuid();
            var ticket = new Ticket();
            var viewModel = new EditTicketViewModel();
            _repository.Setup(r => r.GetTicket(id)).Returns(ticket);
            _viewModelProvider.Setup(p => p.ProvideEditTicketViewModel(ticket)).Returns(viewModel);

            // Act
            var result = _controller.Edit(id) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(viewModel, result.Model);
            _repository.Verify(r => r.GetTicket(id), Times.Once);
            _viewModelProvider.Verify(r => r.ProvideEditTicketViewModel(ticket), Times.Once);
        }

        [Test]
        public void Edit_Should_Change_ViewModelDescription_When_ModelStateIsValid()
        {
            //Arrange
            var viewModel = new EditTicketViewModel();
            var ticket = new Ticket();
            _mapper.Setup(m => m.MapEditViewModel(viewModel)).Returns(ticket);

            //Act
            _controller.Edit(viewModel);

            //Assert
            _repository.Verify(r => r.UpdateTicket(ticket), Times.Once);
            _mapper.Verify(r => r.MapEditViewModel(viewModel), Times.Once);
        }

        [Test]
        public void Edit_Should_ReturnView_When_ModelStateIsInvalid()
        {
            //Arrange
            var viewModel = new EditTicketViewModel { Id = Guid.NewGuid() };
            _controller.ModelState.AddModelError("TestError", @"ErrorMessage");

            //Act
            var result = _controller.Edit(viewModel) as ViewResult;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Edit", result.ViewName);
            _repository.Verify(r => r.UpdateTicket(It.IsAny<Ticket>()), Times.Never);
        }

        [Test]
        public void Delete_Should_Return_Index_When_ModelStateInvalid()
        {
            // Arrange
            _controller.ModelState.AddModelError("TestError", @"ErrorMessage");

            // Act
            var result = _controller.Delete(null) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.ViewName);
            _repository.Verify(r => r.DeleteTicket(It.IsAny<Guid>()), Times.Never);

        }

        [Test]
        public void Delete_Should_Call_DeleteTicket_With_Given_Id()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            _controller.Delete(id);

            // Assert
            _repository.Verify(r => r.DeleteTicket(id), Times.Once);
        }

        [Test]
        public void List_Should_Return_View_With_ViewModels_Matching_Tickets_Gotten_From_Repository()
        {
            // Arrange
            var ticket = new Ticket() {Description = "TestDescription", DueDate = new DateTime(2010, 01, 01)};
            var viewModel = new ListTicketViewModel();
            _repository.Setup(r => r.GetAllTickets()).Returns(new List<Ticket>() {ticket});
            _viewModelProvider.Setup(p => p.ProvideListTicketViewModel(ticket)).Returns(viewModel);
            // Act
            var result = _controller.List() as ViewResult;

            // Assert
            var resultingModel = result.Model as List<ListTicketViewModel>;
            var resultingViewModel = resultingModel.FirstOrDefault();

            Assert.AreEqual(viewModel, resultingViewModel);
        }
    }
}
