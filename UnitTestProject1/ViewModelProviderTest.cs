﻿using System;
using BusinessLogic;
using CustomerSupport.Models;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CustomerSupport.Tests
{
    [TestClass]
    public class ViewModelProviderTest
    {
        private TicketViewModelProvider _provider;

        [TestInitialize]
        public void Setup()
        {
            _provider = new TicketViewModelProvider();
        }

        [TestMethod]
        public void ProvideCreateTicketViewModel_Should_Return_Equivelant_ViewModel()
        {
            //Arrange
            var ticket = new Ticket
            {
                Description = "TestDescription",
                DueDate = new DateTime(2017,06,11)
            };

            //Act
            var viewModel = _provider.ProvideCreateTicketViewModel(ticket);

            //Assert
            Assert.AreEqual(ticket.Description, viewModel.Description);
            Assert.AreEqual(ticket.DueDate, viewModel.DueDate);
        }

        [TestMethod]
        public void ProvideEditTicketViewModel_Should_Return_Equivelant_ViewModel()
        {
            //Arrange
            var ticket = new Ticket
            {
                Description = "TestDescription",
                DueDate = new DateTime(2017, 06, 11)
            };

            //Act
            var viewModel = _provider.ProvideEditTicketViewModel(ticket);

            //Assert
            Assert.AreEqual(ticket.Description, viewModel.Description);
            Assert.AreEqual(ticket.DueDate, viewModel.DueDate);
            Assert.AreEqual(ticket.Id, viewModel.Id);
        }

        [TestMethod]
        public void ProvideListTicketViewModel_Should_Return_Equivelant_ViewModel()
        {
            //Arrange
            var ticket = new Ticket
            {
                Description = "TestDescription",
                DueDate = new DateTime(2017, 06, 11)
            };

            //Act
            var viewModel = _provider.ProvideListTicketViewModel(ticket);

            //Assert
            Assert.AreEqual(ticket.Description, viewModel.Description);
            Assert.AreEqual(ticket.DueDate, viewModel.DueDate);
            Assert.AreEqual(ticket.Id, viewModel.Id);
            Assert.AreEqual(ticket.CreationDateTime, viewModel.CreationDateTime);
        }
    }
}