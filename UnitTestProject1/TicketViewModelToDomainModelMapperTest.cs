﻿using System;
using BusinessLogic;
using CustomerSupport.Models;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CustomerSupport.Tests
{
    [TestClass]
    public class TicketViewModelToDomainModelMapperTest
    {
        private Mock<IRepository> _repository;
        private TicketViewModelToDomainModelMapper _mapper;

        [TestInitialize]
        public void Setup()
        {
            _repository = new Mock<IRepository>();
            _mapper = new TicketViewModelToDomainModelMapper(_repository.Object);
        }

        [TestMethod]
        public void MapCreateViewModel_Should_Return_Equivelant_DomainModel()
        {
            //Arrange
            var viewModel = new CreateTicketViewModel
            {
                Description = "TestDescription",
                DueDate = DateTime.MaxValue
            };

            //Act
            var ticket = _mapper.MapCreateViewModel(viewModel);

            //Assert
            Assert.AreEqual(viewModel.Description, ticket.Description);
            Assert.AreEqual(viewModel.DueDate, ticket.DueDate);
        }

        [TestMethod]
        public void MapEditViewModel_Should_Return_Equivelant_DomainModel()
        {
            //Arrange
            var ticketInRepository = new Ticket();
            var viewModel = new EditTicketViewModel()
            {
                Description = "TestDescription",
                DueDate = DateTime.MaxValue,
                Id = ticketInRepository.Id
            };

            _repository.Setup(r => r.GetTicket(ticketInRepository.Id)).Returns(ticketInRepository);

            //Act
            var ticket = _mapper.MapEditViewModel(viewModel);

            //Assert
            Assert.AreEqual(viewModel.Description, ticket.Description);
            Assert.AreEqual(viewModel.DueDate, ticket.DueDate);
            Assert.AreEqual(viewModel.Id, ticket.Id);
        }
    }
}
