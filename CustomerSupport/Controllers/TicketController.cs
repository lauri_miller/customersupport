﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using BusinessLogic;
using CustomerSupport.Models;

namespace CustomerSupport.Controllers
{

    public class TicketController : Controller
    {
        private readonly IRepository _repository;
        private readonly IViewModelProvider _viewModelProvider;
        private readonly ITicketViewModelToDomainModelMapper _mapper;

        public TicketController(IRepository repository, IViewModelProvider viewModelProvider, ITicketViewModelToDomainModelMapper mapper)
        {
            _repository = repository;
            _viewModelProvider = viewModelProvider;
            _mapper = mapper;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CreateTicketViewModel ticketViewModel)
        {
            if (!ModelState.IsValid) return View("Index");

            var ticket = _mapper.MapCreateViewModel(ticketViewModel);

            _repository.AddTicket(ticket);
            return RedirectToAction("List");
        }

        public ActionResult Edit(Guid? id)
        {
            if (!id.HasValue) return View("Index");

            var ticket = _repository.GetTicket(id.Value);
            var ticketViewModel = _viewModelProvider.ProvideEditTicketViewModel(ticket);

            return View(ticketViewModel);
        }

        [HttpPost]
        public ActionResult Edit(EditTicketViewModel ticketViewModel)
        {
            if (!ModelState.IsValid) return View("Edit");

            var ticket = _mapper.MapEditViewModel(ticketViewModel);

            _repository.UpdateTicket(ticket);

            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            var tickets = _repository.GetAllTickets();
            var ticketViewModels = new List<ListTicketViewModel>();

            foreach (var ticket in tickets)
            {
                var ticketViewModel = _viewModelProvider.ProvideListTicketViewModel(ticket);
                ticketViewModels.Add(ticketViewModel);
            }

            return View(ticketViewModels);
        }

        [HttpPost]
        public ActionResult Delete(Guid? id)
        {
            if (!id.HasValue) return View("Index");

            _repository.DeleteTicket(id.Value);

            return RedirectToAction("List");
        }
    }
}