using System;
using System.ComponentModel.DataAnnotations;

namespace CustomerSupport.Models
{
    public class EditTicketViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Due Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime DueDate { get; set; }
    }
}