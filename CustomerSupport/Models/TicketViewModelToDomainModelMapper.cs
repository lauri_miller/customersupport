﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic;

namespace CustomerSupport.Models
{
    public class TicketViewModelToDomainModelMapper : ITicketViewModelToDomainModelMapper
    {
        private readonly IRepository _repository;

        public TicketViewModelToDomainModelMapper(IRepository repository)
        {
            _repository = repository;
        }

        public Ticket MapCreateViewModel(CreateTicketViewModel viewModel)
        {
            var ticket = new Ticket
            {
                Description = viewModel.Description,
                DueDate = viewModel.DueDate
            };

            return ticket;
        }

        public Ticket MapEditViewModel(EditTicketViewModel viewModel)
        {
            var ticket = _repository.GetTicket(viewModel.Id);

            ticket.Description = viewModel.Description;
            ticket.DueDate = viewModel.DueDate;

            return ticket;
        }
    }
}