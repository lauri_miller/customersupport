using BusinessLogic;

namespace CustomerSupport.Models
{
    public interface IViewModelProvider
    {
        CreateTicketViewModel ProvideCreateTicketViewModel(Ticket ticket);
        EditTicketViewModel ProvideEditTicketViewModel(Ticket ticket);
        ListTicketViewModel ProvideListTicketViewModel(Ticket ticket);
    }
}