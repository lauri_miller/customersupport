﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic;

namespace CustomerSupport.Models
{
    public class TicketViewModelProvider : IViewModelProvider
    {
        public CreateTicketViewModel ProvideCreateTicketViewModel(Ticket ticket)
        {
            var viewModel = new CreateTicketViewModel
            {
                Description = ticket.Description,
                DueDate = ticket.DueDate
            };

            return viewModel;
        }

        public EditTicketViewModel ProvideEditTicketViewModel(Ticket ticket)
        {
            var viewModel = new EditTicketViewModel
            {
                Description = ticket.Description,
                DueDate = ticket.DueDate,
                Id = ticket.Id
            };

            return viewModel;
        }

        public ListTicketViewModel ProvideListTicketViewModel(Ticket ticket)
        {
            var viewModel = new ListTicketViewModel
            {
                CreationDateTime = ticket.CreationDateTime,
                Description = ticket.Description,
                DueDate = ticket.DueDate,
                Id = ticket.Id
            };

            return viewModel;
        }
    }
}