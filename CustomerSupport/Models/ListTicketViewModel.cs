﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BusinessLogic;

namespace CustomerSupport.Models
{
    public class ListTicketViewModel
    {
        public Guid Id { get; set; }

        public bool Urgent => (DueDate - DateTime.Now).TotalMinutes < 60;

        [Display(Name = "Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Due Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime DueDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Time of submission")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CreationDateTime { get; set; }
    }
}