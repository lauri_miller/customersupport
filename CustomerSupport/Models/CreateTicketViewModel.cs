using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CustomerSupport.Models
{
    public class CreateTicketViewModel : IValidatableObject
    {
        [Required]
        [Display(Name = "Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Due Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime DueDate { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (DueDate < DateTime.Now)
            {
                yield return new ValidationResult("Due date cannot be in the past.", new[] { "DueDate" });
            }
        }
    }
}