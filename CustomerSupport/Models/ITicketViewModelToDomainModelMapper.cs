﻿using BusinessLogic;

namespace CustomerSupport.Models
{
    public interface ITicketViewModelToDomainModelMapper
    {
        Ticket MapCreateViewModel(CreateTicketViewModel viewModel);
        Ticket MapEditViewModel(EditTicketViewModel viewModel);
    }
}