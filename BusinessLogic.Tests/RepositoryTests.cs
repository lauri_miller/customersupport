﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Api;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace BusinessLogic.Tests
{
    [TestFixture]
    public class RepositoryTests
    {
        private Mock<CustomerSupportDb> _customerSupportDbMock;
        private Repository _repository;
        [SetUp]
        public void Setup()
        {
            _customerSupportDbMock = new Mock<CustomerSupportDb>();
            _repository = new Repository(_customerSupportDbMock.Object);
        }

        [Test]
        public void GetAllTicets_Should_Return_All_Tickets()
        {
            // Arrange
            var tickets = new[] { new Ticket(), new Ticket() };
            var ticketsDbSetMock = new FakeDbSet<Ticket>(tickets);
            _customerSupportDbMock.Setup(c => c.Tickets).Returns(ticketsDbSetMock);

            // Act
            var result = _repository.GetAllTickets();

            // Assert
            NUnit.Framework.CollectionAssert.AreEqual(tickets, result.ToList());
            _customerSupportDbMock.VerifyGet(d => d.Tickets,Times.AtLeastOnce);
        }
        
        [Test]
        public void DeleteTicket_Should_RemoveTicket_When_Deleted()
        {
            var ticket = new Ticket();
            var ticketsDbSetMock = new Mock<DbSet<Ticket>>();
            _customerSupportDbMock.Setup(c => c.Tickets).Returns(ticketsDbSetMock.Object);
            ticketsDbSetMock.Setup(s => s.Find(ticket.Id)).Returns(ticket);

            //Act
            _repository.DeleteTicket(ticket.Id);

            //Assert
            ticketsDbSetMock.Verify(x => x.Remove(ticket), Times.Once);
            _customerSupportDbMock.Verify(d => d.SaveChanges(), Times.Once);
        }

        [Test]
        public void GetTicket_Should_Return_Ticket_With_Given_Id()
        {
            // Arrange
            var ticket = new Ticket();
            var id = ticket.Id;
            var tickets = new[] {ticket, new Ticket() };
            var ticketsDbSetMock = new FakeDbSet<Ticket>(tickets);
            _customerSupportDbMock.Setup(c => c.Tickets).Returns(ticketsDbSetMock);

            // Act
            var result = _repository.GetTicket(id);

            // Assert
            Assert.AreEqual(ticket, result);
        }

        [Test]
        public void AddTicket_Should_Call_Add_With_Right_Ticket()
        {
            // Arrange
            var ticket = new Ticket();
            var ticketsDbSetMock = new Mock<DbSet<Ticket>>();
            _customerSupportDbMock.Setup(c => c.Tickets).Returns(ticketsDbSetMock.Object);

            // Act
            _repository.AddTicket(ticket);

            // Assert
            ticketsDbSetMock.Verify(t => t.Add(ticket), Times.Once);
            _customerSupportDbMock.Verify(d => d.SaveChanges(), Times.Once);
        }

        [Test]
        public void UpdateTicket_Should_Access_Entry_State_And_Call_Update()
        {
            // Arrange
            var ticket = new Ticket();
            var ticketsDbSetMock = new Mock<DbSet<Ticket>>();
            _customerSupportDbMock.Setup(c => c.Tickets).Returns(ticketsDbSetMock.Object);
            //_customerSupportDbMock.Setup(d => d.Entry(ticket)).Returns(new DbEntityEntry<Ticket>());

            // Act
            _repository.UpdateTicket(ticket);

            // Assert
            _customerSupportDbMock.Verify(d => d.SaveChanges(), Times.Once);
            //Assert.AreEqual(_customerSupportDbMock.Object.Entry(ticket).State, EntityState.Modified); Only 'modified' when mocked, otherwise unchanged
        }
    }
}