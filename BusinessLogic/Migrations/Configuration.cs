namespace BusinessLogic.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BusinessLogic.CustomerSupportDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "BusinessLogic.CustomerSupportDb";
        }

        protected override void Seed(BusinessLogic.CustomerSupportDb context)
        {

        }
    }
}
