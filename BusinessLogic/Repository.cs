﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace BusinessLogic
{
    public class Repository : IRepository
    {
        private readonly CustomerSupportDb _db;

        public Repository(CustomerSupportDb db)
        {
            _db = db;
        }
        public IReadOnlyCollection<Ticket> GetAllTickets()
        {
            var tickets = _db.Tickets;
            return tickets.ToList();
        }

        public void DeleteTicket(Guid id)
        {
            var ticketToRemove = _db.Tickets.Find(id);
            if (ticketToRemove != null) _db.Tickets.Remove(ticketToRemove);
            _db.SaveChanges();
        }

        public Ticket GetTicket(Guid id)
        {
            var ticket = _db.Tickets.SingleOrDefault(x => x.Id == id);
            return ticket;
        }

        public void UpdateTicket(Ticket ticket)
        {
            _db.Entry(ticket).State = System.Data.Entity.EntityState.Modified;
            _db.SaveChanges();
        }

        public void AddTicket(Ticket ticket)
        {
            _db.Tickets.Add(ticket);
            _db.SaveChanges();
        }
    }
}
