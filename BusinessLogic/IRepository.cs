﻿using System;
using System.Collections.Generic;

namespace BusinessLogic
{
    public interface IRepository
    {
        IReadOnlyCollection<Ticket> GetAllTickets();

        void DeleteTicket(Guid id);

        Ticket GetTicket(Guid id);

        void UpdateTicket(Ticket ticket);

        void AddTicket(Ticket ticket);
    }
}