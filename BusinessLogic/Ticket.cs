﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Ticket
    {
        public Ticket()
        {
            Id = Guid.NewGuid();
            CreationDateTime = DateTime.Now;
        }

        public virtual Guid Id { get; private set; }

        public virtual string Description { get; set; }

        public virtual DateTime DueDate { get; set; }

        public virtual DateTime CreationDateTime { get; private set; }
    }
}
